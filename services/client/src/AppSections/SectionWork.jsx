import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// @material-ui/icons

// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Button from "components/CustomButtons/Button.jsx";

import workStyle from "assets/jss/material-kit-pro-react/views/landingPageSections/workStyle.jsx";

class SectionWork extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.section}>
        <GridContainer justify="center">
          <GridItem cs={12} sm={8} md={8}>
            <h2 className={classes.title}>Made by one person, built with many tools.</h2>
            <h4 className={classes.description}>
              <a href={"https://nodejs.org"}>Node</a>, <a href="https://python.org">Python</a>, <a href={"https://www.postgresql.org/"}>PostgreSQL</a>

              <br /><br /><a href={"https://www.docker.com/community/open-source"}>Docker</a>, <a href="https://about.gitlab.com/product/continuous-integration/">GitLab CI/CD</a>, <a href="https://www.terraform.io/">Terraform</a> and <a href="https://kubernetes.io/">Kubernetes</a>.

              <br /><br />Take a look around the <a href={"https://gitlab.com/amdingo/rp-dev-site"}>GitLab repository</a> for this site.  If you have any questions, suggestions or requests please submit them below.  If you find any bugs, please report them <a href={"https://gitlab.com/amdingo/rp-dev-site/issues"}>here</a>.
            </h4>
            <form>
              <GridContainer>
                <GridItem xs={12} sm={6} md={6}>
                  <CustomInput
                    labelText="Your Name"
                    id="name"
                    formControlProps={{
                      fullWidth: true
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={6} md={6}>
                  <CustomInput
                    labelText="Your Email"
                    id="email"
                    formControlProps={{
                      fullWidth: true
                    }}
                  />
                </GridItem>
                <CustomInput
                  labelText="Your Message"
                  id="message"
                  formControlProps={{
                    fullWidth: true,
                    className: classes.textArea
                  }}
                  inputProps={{
                    multiline: true,
                    rows: 5
                  }}
                />
                <GridItem
                  xs={12}
                  sm={4}
                  md={4}
                  className={`${classes.mrAuto} ${classes.mlAuto}`}
                >
                  <Button color="primary">Send Message</Button>
                </GridItem>
              </GridContainer>
            </form>
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

export default withStyles(workStyle)(SectionWork);
