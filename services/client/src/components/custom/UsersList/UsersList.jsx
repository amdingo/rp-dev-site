import React from 'react';
import Card from 'components/Card/Card';
import CardBody from 'components/Card/CardBody';

const UsersList = (props) => {
  return (
    <div>
      {
        props.users.map((u) => {
          return (
            <Card
              key={u.id}
            >
              <CardBody>{ u.username }</CardBody>
            </Card>
          )
        })
      }
    </div>
  )
};

export default UsersList
