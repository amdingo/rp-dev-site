import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from "react-router-dom";

import withStyles from "@material-ui/core/styles/withStyles";
import typographyStyle from "assets/jss/material-kit-pro-react/components/typographyStyle.jsx";
import App from 'App';

const StyledApp = withStyles(typographyStyle)(App);

ReactDOM.render(
  <Router>
    <StyledApp/>
  </Router>,
  document.getElementById('root')
);
